/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef F16_H_
#define F16_H_

#include <stdint.h>

extern uint16_t f16_enc(double);
extern double f16_dec(uint16_t);

#endif
