#include "w32dll.h"

#include <windows.h>

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef int main_fn(int, char **);

void *w32dll_load_from_rpath(const wchar_t *rpath)
{
    static wchar_t full_path[MAX_PATH];

    size_t rpath_len = wcslen(rpath);
    uint32_t modpath_len = GetModuleFileNameW(NULL, full_path, MAX_PATH);

    if(modpath_len == 0 || modpath_len + rpath_len + 5 >= MAX_PATH)
        return NULL;

    memcpy(full_path + modpath_len, L"\\..\\", 4 * sizeof (wchar_t));
    memcpy(full_path + modpath_len + 4, rpath, rpath_len * sizeof (wchar_t));

    return LoadLibraryW(full_path);
}

int w32dll_call_as_main(void *dll, const char *name)
{
    main_fn *dllmain = (main_fn *)GetProcAddress(dll, name);
    if(!dllmain) abort();

    int argc;

    wchar_t **wargv = CommandLineToArgvW(GetCommandLineW(), &argc);
    if(!wargv) abort();

    char *argv[argc + 1];
    argv[argc] = NULL;

    size_t size = 0;
    for(int i = 0; i < argc; ++i)
        size += wcslen(wargv[i]) * 4 + 1;

    char *buffer = malloc(size);
    if(!buffer) abort();

    size_t pos = 0;
    for(int i = 0, len; i < argc; ++i, pos += len)
    {
        argv[i] = buffer + pos;
        len = WideCharToMultiByte(CP_UTF8, 0, wargv[i], -1, argv[i],
                size - pos, NULL, NULL);
        if(!len) abort();
    }

    buffer = realloc(buffer, pos);
    if(!buffer) abort();

    LocalFree(wargv);
    int rv = dllmain(argc, argv);
    free(buffer);

    return rv;
}
