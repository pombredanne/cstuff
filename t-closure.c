/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "closure.h"

#undef NDEBUG
#include <assert.h>

static void reply(void *p)
{
	*(int *)p = 42;
}

static void reset(void *p)
{
	*(int *)p = 0;
}

int main(void)
{
	{
		static int answer;
		int id_reply = -1, id_reset = -1;

		void (*clo_reply)(void) = closure_create(&id_reply, reply, &answer);
		assert(clo_reply);
		assert(id_reply == 0);

		void (*clo_reset)(void) = closure_create(&id_reset, reset, &answer);
		assert(clo_reset);
		assert(id_reset == 1);

		clo_reply();
		assert(answer == 42);
		closure_destroy(id_reply);

		clo_reset();
		assert(answer == 0);
		closure_destroy(id_reset);
	}

	return 0;
}
