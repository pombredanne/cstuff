/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "f16.h"
#include <math.h>

#undef NDEBUG
#include <assert.h>

int main(void)
{
	assert(f16_dec(0x3C00) == 1.0);
	assert(f16_dec(0x3C01) == 1.0 + 1.0 / (1 << 10));
	assert(f16_dec(0xC000) == -2.0);
	assert(f16_dec(0x7BFF) == 65504.0);

	assert(f16_dec(0x0400) == 1.0 / (1 << 14));
	assert(f16_dec(0x03FF) == 1.0 / (1 << 14) - 1.0 / (1 << 24));
	assert(f16_dec(0x0001) == 1.0 / (1 << 24));

	assert(f16_dec(0x0000) == 0.0);
	assert(signbit(f16_dec(0x0000)) == 0);

	assert(f16_dec(0x8000) == 0.0);
	assert(signbit(f16_dec(0x8000)) == 1);

	assert(f16_dec(0x7C00) == INFINITY);
	assert(f16_dec(0xFC00) == -INFINITY);
	assert(isnan(f16_dec(0x7E00)));

	static const double ONE_THIRD_F16 = 1.0 / (1 << 2) + 1.0 / (1 << 4) +
		1.0 / (1 << 6) + 1.0 / (1 << 8) + 1.0 / (1 << 10) + 1.0 / (1 << 12);
	assert(fabs(ONE_THIRD_F16 - 1.0 / 3.0) < 1e-4);
	assert(f16_dec(0x3555) == ONE_THIRD_F16);

	return 0;
}
