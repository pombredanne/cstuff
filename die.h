#ifndef DIE_H_
#define DIE_H_

#include <stdio.h>
#include <stdlib.h>

#define die(...) (fprintf(stderr, __VA_ARGS__), exit(EXIT_FAILURE))

#endif
